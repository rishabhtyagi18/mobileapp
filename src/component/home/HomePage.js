import React, { useState } from 'react';
import "../home/HomePage.css";

function HomePage() {

    return (
      <div>
           <div className="padding">
               <div className="name">Sonali Gupta</div>
           </div>
          
           
       <img src="https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg" className="img" />

       <h3 className="h3">About Sonali Gupta</h3>
       <div className="card">
        <div className="contain">
            <div className="cardAlign">
            <p className="description">I am glad you are interested in my profile. I am pursuing law  and I am residing in India.I love traveling and exploring new places.I am searching for a best friend with whom I can share the rest of my life and fulfill our dreams together.I love cooking, gardening, singing and dancing.</p>
            </div>
        </div>
       </div>

       <h3 className="h3">Lifestyle & Apperance</h3>
       <div className="card1">
           <div className="contain">
               <div className="cardAlign">
                    <div>
                        <div className="flex">
                            <img src="https://img.icons8.com/metro/26/000000/user-female-circle.png" />
                            <div className="profile">24 yrs, 5' 5", Virgo</div>
                        </div>
                        <div className="flex lifeApperance">
                            <img src="https://img.icons8.com/metro/26/000000/marker.png" />
                            <div className="profile">Lives in Gurugram, Delhi-NCR, India</div>
                        </div>
                        <div className="flex lifeApperance">
                            <img src="https://img.icons8.com/metro/26/000000/calendar-12.png" />
                            <div className="profile">24th August, 1993</div>
                        </div>
                        <div className="flex lifeApperance">
                            <img src="https://img.icons8.com/metro/26/000000/gender--v1.png" />
                            <div className="profile">Female</div>
                        </div>

                    </div>
               </div>

           </div>
       </div>

      </div>
    );
  }
  
  export default HomePage;