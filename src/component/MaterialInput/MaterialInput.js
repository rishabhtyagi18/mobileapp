import React from 'react';
import './MaterialInput.css';

const MaterialInput = (props) => {
  const inputChangeHandler = props.onChange ? props.onChange : () => { console.log('no onchange handler passed'); };
  const placeholder = props.placeholder ? props.placeholder : 'Sample';
  return (
    <>
      <div className="mainContainer">
        <div className="container" style={props.style}>
          <input
            className="input"
            {...props.inputProps}
            onChange={inputChangeHandler}
            placeholder={placeholder}
          />
          <div className="label">{placeholder}</div>
        </div>
        {props.rightSideText
          ? (
            <span
              onClick={props.rightSideTextClick ? props.rightSideTextClick : () => false}
              className="rightSideText"
              style={{...(props.rightSideTextStyle ? props.rightSideTextStyle : {})}}
            >
              {props.rightSideText}
            </span>
          )
          : null}
      </div>
    </>
  );
};

export default MaterialInput;
