import React, { useState, useEffect } from 'react';
import "../profile/Profile.css";
import MaterialInput from '../MaterialInput/MaterialInput';
import { useHistory } from "react-router-dom";

function Profile() {
  const [focus, setfocus] = useState('');

  let history = useHistory();
  const [name, setName] = useState(localStorage.getItem("name"));
  const [lastName, setLastName] = useState(localStorage.getItem("lastName"));
  const [gender, setGender] = useState(localStorage.getItem("gender" || ""));
  const [date, setDate] = useState(localStorage.getItem("date" || ""));

  const dd = gender ? "female" : "male";

  function submit() {
    if(name !== "undefined" && lastName !== "")
    {
      localStorage.setItem('name', name);
      localStorage.setItem('lastName', lastName);
      localStorage.setItem('gender', dd);
      localStorage.setItem('date', date);
          history.push("/home");
          return;
  }
    else{
      alert('Please enter All The Details.');
    }
  }
  const handleChange = e => {
    
      setGender(e.target.gender.value)
  };


    return (
      <div>
        <div className="heading">What's your name?</div>
        <div className="align">
          <div  className="inputFieldBox">
            <MaterialInput
                inputProps={{
                  type: 'id',
                  maxLength: 10,
                  value: name
                }}
                style={{ height: '2.575rem', borderRadius: '8px' }}
                placeholder="First Name"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
          </div>
          <div  className="inputFieldBox1">
            <MaterialInput
                inputProps={{
                  type: 'id',
                  maxLength: 10,
                  value: lastName
                }}
                style={{ height: '2.575rem', borderRadius: '8px' }}
                placeholder="Last Name"
                onChange={(e) => {
                  setLastName(e.target.value);
                }}
              />
          </div>

          <div className="genHeading">And  your gender?</div>

          <div className="sliderBox">
          
              <div value="male"  className="Options Admin btn" onChange={handleChange}>Male</div>
              <div value="female" className="Options1 Admin btn" onChange={handleChange}>Female</div>
            
          </div>

          <div className="dobText">What's your date of birth?</div>
          <div className="subHed">This can't be changes later</div>
          
          <div  className="inputFieldBox2">
            <div >
              {/* <input type="text" className="input" onChange={(e) => {setDate(e.target.value);}} placeholder="DD/MM/YYYY" /> */}

              <MaterialInput
                inputProps={{
                  type: 'date',
                  maxLength: 10,
                  value: date
                }}
                style={{ height: '2.575rem', borderRadius: '8px' }}
                placeholder="DD/MM/YYYY"
                onChange={(e) => {setDate(e.target.value);}}
              />
              </div>
          </div>

        </div>

        <div className="btnAlign" onClick={submit}>
            <button className="submit"><span className="btnIcon">&#8250;</span></button>
          </div>
      </div>
    );
  }
  
  export default Profile;