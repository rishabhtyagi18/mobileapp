import React, { useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import "../app/App.css";
import MaterialInput from '../MaterialInput/MaterialInput';
import { useHistory } from "react-router-dom";

function App(props) {
  
  let history = useHistory();

  const [id, setId] = useState("");
  const [password, setPassword] = useState("");

  function login() {
    if(id !== "undefined" && password !== "")
    {
          localStorage.setItem('ids', id);
          localStorage.setItem('pass', password);
          history.push("/my-profile");
          return;
  }
    else{
      alert('Please enter Both The Details.');
    }
  }


  return (
    <>
     <div className="App">
      <header className="App-header">
        <div className="pwa">PWA</div>
        <div className="catchphrase">Catchphrase</div>

      <div className="align">
        <div  className="inputFieldBox">
          <MaterialInput
              inputProps={{
                type: 'id',
                maxLength: 10,
                value: localStorage.getItem("ids"),
              }}
              style={{ height: '2.575rem' }}
              placeholder="Login Id"
              onChange={(e) => {
                setId(e.target.value);
              }}
            />
        </div>
        <div  className="inputFieldBox1">
          <MaterialInput
              inputProps={{
                type: 'tel',
                value: localStorage.getItem("pass"),
              }}
              style={{ height: '2.575rem' }}
              placeholder="Password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
        </div>

        <div className="stepButton proceedButton" onClick={login}>
          <p className="stepButtonText">
            Log In
          </p>
        </div>
      </div>
        
      </header>
    </div>
    </>
  );
}

export default withRouter(App);
