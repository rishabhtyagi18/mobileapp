import React from 'react';
import App from './App';

const title = 'App';

function action() {
    return {
      chunks: ['app'],
      title,
      component: (
        <App /> 
      ),
    };
  }

export default action;
