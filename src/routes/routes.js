import React, { Suspense, lazy } from "react";
import { Route, Switch, BrowserRouter as Router, Redirect } from "react-router-dom";

const OrderHistory = lazy(() => import("../component/app/App"));
// const NotFound = lazy(() => import("../Components/error/error.js"));
const MyProfile = lazy(() => import("../component/profile/Profile"));
const Gallery = lazy(() => import("../component/home/HomePage"));

const RoutingComponent = props => {
  return (
    <Suspense fallback={<div></div>}>
      <Router>
        <Switch>
        <Route exact path="/" render={() => <Redirect to="/login" />} />
        <Route exact path="/login" component={OrderHistory} />
        <Route exact path="/my-profile" component={MyProfile} />
        <Route exact path="/home" component={Gallery} />
          {/* <Route exact path="*" component={NotFound} /> */}
          
        </Switch>
      </Router>
    </Suspense>
  );
};

export default RoutingComponent;
